#ifndef BOUNDARY_H
#define BOUNDARY_H

#include "LatticeSite.h"


class Boundary{
protected:
  int nbNodes;
  int **nodes;
public:
  Boundary();
  virtual void FreeSlipBC(VelSite **sites, VelSite **_sites) = 0;
  virtual void HalfWayFreeSlipBC(VelSite **sites, VelSite **_sites) = 0;
  virtual void TemperatureBC(VelSite **velSites, ThermalSite **thermalSites,
		     double **T, double ***u) = 0;
  
};

class TopWall : public Boundary{
 private:
 public:
  TopWall(const int d[2]);
  virtual void FreeSlipBC(VelSite **sites, VelSite **_sites);
  virtual void HalfWayFreeSlipBC(VelSite **sites, VelSite **_sites);
  virtual void TemperatureBC(VelSite **velSites, ThermalSite **thermalSites,
		     double **T, double ***u);
  void HalfWayBounceBack(VelSite **sites, VelSite **_sites);

};

/*Could be useful if we need to replace to topo. by a flat wall for testing*/

/* class BottomWall : public Boundary{ */
/*  private: */
/*  public: */
/*   BottomWall(const int d[2]); */
/*   void FreeSlipBC(VelSite **sites, VelSite **_sites); */
/*   virtual void HalfWayFreeSlipBC(VelSite **sites, VelSite **_sites); */
/*   void TemperatureBC(VelSite **velSites, ThermalSite **thermalSites, */
/* 		     double **T, double ***u); */
/*   void HalfWayBounceBack(VelSite **sites, VelSite **_sites); */

/* }; */

class Topography : public Boundary{
 private:
  int *lbl;
 public:
  Topography(const int d[2], int h, double*** u, VelSite**, VelSite**);
  virtual void FreeSlipBC(VelSite **sites, VelSite **_sites);
  virtual void HalfWayFreeSlipBC(VelSite **sites, VelSite **_sites);
  virtual void TemperatureBC(VelSite **velSites, ThermalSite **thermalSites,
		     double **T, double ***u);
};

#endif
